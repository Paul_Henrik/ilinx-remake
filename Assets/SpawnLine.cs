﻿using UnityEngine;
using System.Collections;

public class SpawnLine : MonoBehaviour
{
    public float spawnTimer = 5;
    private float countDownPriv;
    private int rndSpawn;
    private int rndObj;
    public int zeppelinSpawner = 10;
    public GameObject[] spawnPoint;
    public GameObject[] Enemy;
    public GameObject zeppelin;
    
	// Use this for initialization
	void Start ()
	{
        countDownPriv = spawnTimer;
    }

	// Update is called once per frame
	void Update ()
	{
        spawnCircle();
        spawnZeppelin();
	}

    void spawnCircle()
    {
        //spawn from a rnd place.
        rndSpawn = Random.Range(0, spawnPoint.Length);
        rndObj = Random.Range(0, 2);

        countDownPriv -= 1f * Time.deltaTime;
        if (countDownPriv < 0)
        {
            Instantiate(Enemy[rndObj], spawnPoint[rndSpawn].transform.position, Quaternion.identity);
            countDownPriv = spawnTimer;
        }
        if (GameMaster.raiseLevel == true)
        {
            Debug.Log("Raising spawnRate");
            if(spawnTimer > 0.5f)
            spawnTimer -= 0.2f;
            GameMaster.raiseLevel = false;
        }

    }

    void spawnZeppelin()
    {
        if (GameMaster.highScore > zeppelinSpawner)
        {
            zeppelinSpawner += 1000;
            Instantiate(zeppelin, spawnPoint[5].transform.position, Quaternion.identity);
        }
    }
}