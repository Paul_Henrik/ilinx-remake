﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    private int rnd;                    //Random number
    public int randomRange;             //Stupid name for number for adding higher blocks to jump
    public int height;                  //Height blocks will go to
    public int plusHeight;
    public int maxHeight;               //Maximum Height
    private float spawnTimer = 1f;      // NEW
    public float _spawnTimer = 1f;
    private int stepBasedDifficulty;    //Adds more height or something every 100 steps the player take
    public int maxStepBasedDifficulty; //Maximum steps before it resets; Controls how long "resting" zone the player get
    public GameObject[] spawnPoint;     //GameObject spawnPosition;
    public GameObject[] spawnObject;    //Gameobject Type Dirt, Grass etc.
    private GameObject clonedObject;    //Object allready instatiated
    private GameObject clonedObject2nd;

    public int addStepsAtStart;         //For play testing
    private int addstepstoheight;
    void Start()
    {
        maxHeight = 2;
        ScoreManager.AddPoints(addStepsAtStart);
        if (addStepsAtStart > 0)
        {
            if (addstepstoheight >= 300)
                addstepstoheight = 300;
            addstepstoheight = addStepsAtStart /= 100;
            maxHeight += addstepstoheight;
        }
            


    }


    void Update()
    {
        spawnCircle();
        
    }
    public void spawnCircle()
    {

        {
            spawnTimer -= Time.deltaTime;
            if (spawnTimer < 0.0f)
            {
                spawnTimer = _spawnTimer;

                if (ScoreManager.score <= 500)
                firstSpawnFase();
                else
                secondSpawnFase();
                
            }
        }
    }

    public void firstSpawnFase()                                        //Affects the first 500steps
    {
        if (ScoreManager.score >= 400)
        {
            if (height <= 1)
                rnd = Random.Range(0, 2) + randomRange;
            else
                rnd = Random.Range(-2, 2) + randomRange;
        }
        else
            rnd = Random.Range(-2, 2) + randomRange;

        height += rnd;
        checkHeightSecurity();                                          //Checks if height goes over max or min Height;

        checkActive();
        ScoreManager.AddPoints(1);
        stepBasedDifficulty++;

        if (stepBasedDifficulty >= 100 && ScoreManager.score <= 300)
        {
            addHeight(1);
            stepBasedDifficulty = 0;
        }

    }

    public void secondSpawnFase()                                       //Affects after 500steps
    {
        if (stepBasedDifficulty <= maxStepBasedDifficulty / 3)                                 //Makes the terrain harder
        {
            Debug.Log("Hardmode On");
            if (height <= 1)
                rnd = Random.Range(0, 2) + randomRange;
            else
                rnd = Random.Range(-2, 2) + randomRange;
        }
        else
            rnd = Random.Range(-2, 2) + randomRange;

        height += rnd;
        checkHeightSecurity();

        checkActive();

        ScoreManager.AddPoints(1);
        stepBasedDifficulty++;

        if (stepBasedDifficulty >= maxStepBasedDifficulty)
        {
            stepBasedDifficulty = 0;
            rnd = Random.Range(10, 45);
            maxStepBasedDifficulty = rnd + 50;
        }
    }

    public void checkActive() //checks which spawns are active. If they are active, they will spawn one brick
    {
        
        for (int i = 0; i < spawnPoint.Length; i++)
        {
            if (i < height)
            {
                spawnPoint[i].gameObject.SetActive(true);
                clonedObject2nd = Instantiate(spawnObject[0], spawnPoint[i].transform.position, Quaternion.identity) as GameObject;
                if (rnd == 2 && i == (height - 1))
                {
                    Debug.Log("rnd 2 is happening");
                    clonedObject2nd.transform.GetChild(0).gameObject.SetActive(true);
                }
            }
            else if (i == height)
            {            
                if (rnd == 2)
                {
                    clonedObject = Instantiate(spawnObject[1], spawnPoint[i].transform.position, Quaternion.identity) as GameObject; //The object that needs deactivation
                    clonedObject.GetComponent<BoxCollider2D>().enabled = false;
                }
                else
                {
                    Instantiate(spawnObject[1], spawnPoint[i].transform.position, Quaternion.identity);
                }
            }
            else
            {
                spawnPoint[i].gameObject.SetActive(false);
            }

        }
    }



    public void addHeight(int additioHeight)
    {
        maxHeight += additioHeight;
    }

    public void checkHeightSecurity()
    {
        if (height >= maxHeight)
            height = maxHeight;
        if (height <= 0)
            height = 0;
    }
}