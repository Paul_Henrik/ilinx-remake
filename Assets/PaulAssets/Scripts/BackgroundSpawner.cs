﻿using UnityEngine;
using System.Collections;

public class BackgroundSpawner : MonoBehaviour
{
    public GameObject[] Object;
    public GameObject[] SpawnPoint;
    private float countDown;
    public float staticCountDown = 15;
    // Use this for initialization
    void Start()
    {
        countDown = 0;
    }

    // Update is called once per frame
    void Update()
    {
        countDown -= Time.deltaTime;
        if (countDown <= 0)
        {
            spawn(0);
            spawnIsland();
            countDown = staticCountDown;
        }
    }

    void spawn(int element)
    {
        Instantiate(Object[element], SpawnPoint[0].transform.position, Quaternion.identity);
    }
    void spawnIsland()
    {
        int element = Random.Range(1, Object.Length);
        int spawn = Random.Range(1, SpawnPoint.Length);
        Instantiate(Object[element], SpawnPoint[spawn].transform.position, Quaternion.identity);
    }
}