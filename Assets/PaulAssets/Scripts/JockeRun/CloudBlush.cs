﻿using UnityEngine;
using System.Collections;

public class CloudBlush : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public Sprite blush;
	// Use this for initialization
	void Start ()
	{
        spriteRenderer = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void Update ()
	{

	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            changeSprite();
            PlayerMovement.blushingCloud = true;
        }
    }

    private void changeSprite()
    {
        spriteRenderer.sprite = blush;
    }
}