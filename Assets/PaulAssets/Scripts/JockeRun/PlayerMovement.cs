﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public float jumpHeight = 15f;
    public float jumpCounter = 0;
    public float jumpCounterCoolDown;

    public Transform groundCheck;       //Checks ground for jump
    public float groundCheckRadius;
    public LayerMask whatisGround;
    public bool grounded;

    private Rigidbody2D rb2d;

    public float speed;               //playerSpeed;
    private float prevSpeed;
    public static bool blushingCloud; //If player touches a cloud he will get extra speed =)
    bool coRun;
    private Transform ps;
    private Transform ps2;
    public Animator anim;

    void Start()
    {
        ps = gameObject.transform.GetChild(0);
        ps2 = gameObject.transform.GetChild(1);
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        prevSpeed = speed;
        
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatisGround); //Checks if player is standing on the ground. Also the surface objects needs to be in a layer called "ground"
    }

    // Update is called once per frame
    void Update()
    {
        jump();
        speedController();
        death();
        if (grounded)
            anim.SetBool("Grounded", grounded);
    }

    void jump()
    {
        jumpCounterCoolDown -= 1f * Time.deltaTime;
        if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && grounded)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpHeight);
            jumpCounter++;
            jumpCounterCoolDown = 1f; 
        }
        if (jumpCounterCoolDown <= 0)
        {
            jumpCounter = 0;
            jumpCounterCoolDown = 0;
        }
            

    }

    void speedController()
    {
        if (transform.position.x < 0)
        {
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
            if (blushingCloud)
            {
                StartCoroutine("coblushingCloud");
                blushingCloud = false;
            }
        }
        if ((jumpCounter >= 10) && (!coRun))
        {
            coRun = true;
            ps2.gameObject.SetActive(true);
            StartCoroutine("coExhausted");
        }
        else if (jumpCounter >= 6)
        {
            ps.gameObject.SetActive(true);
        }
        else if (jumpCounter < 5)
        {
            ps.gameObject.SetActive(false);
            ps2.gameObject.SetActive(false);
        }
       


    }

    public IEnumerator coblushingCloud()
    {
        float _speedAdd = 1f;

        speed =+ _speedAdd;
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
        yield return new WaitForSeconds(2);
        speed = prevSpeed;
    }

    public IEnumerator coExhausted()
    {
        Debug.Log("SlowRunner");
        float _speedDecrease = 2f;
        yield return speed -= _speedDecrease;
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
        yield return new WaitForSeconds(2);
        speed = prevSpeed;
        jumpCounter = 0;
        coRun = false;
    }

    void death()
    {
        if ((transform.position.y < -4) || (transform.position.x < -9))
        {
            LevelManager.isPlayerAlive = false;
        }
    }

}