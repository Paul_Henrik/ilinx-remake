﻿using UnityEngine;
using System.Collections;

public class DestroyThisObject : MonoBehaviour
{
    public bool mountain;
    public bool timeDestroy;
    public float time;

	void Update ()
	{
        if (timeDestroy)
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {


                if (mountain == true)
                {
                    LevelManager.MountainAmount--;
                }

                Destroy(gameObject);
            }
        }
            

	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "KillBox")
            Destroy(gameObject);
    }
}