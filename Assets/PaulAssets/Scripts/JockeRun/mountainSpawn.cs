﻿using UnityEngine;
using System.Collections;

public class mountainSpawn : MonoBehaviour
{
    public GameObject[] Mountains;
    public GameObject SpawnPoint;
    private float countDown;
    public float staticCountDown = 15;
    public int maxMountains = 2;
    // Use this for initialization
    void Start()
    {
        countDown = 0;
    }

    // Update is called once per frame
    void Update()
    {
        countDown -= Time.deltaTime;
        if (countDown <= 0)
        {
            spawn(0);
            countDown = staticCountDown;
        }
    }

    void spawn(int element)
    {
        Instantiate(Mountains[element], SpawnPoint.transform.position, Quaternion.identity);
    }
}