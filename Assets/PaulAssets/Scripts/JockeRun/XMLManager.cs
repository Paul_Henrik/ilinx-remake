﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;  //Use lists
using System.Xml;                  //basic xml attributes
using System.Xml.Serialization;    //acces xml serialiser
using System.IO;                    //File management
public class XMLManager : MonoBehaviour
{
    public static XMLManager ins;

    void Start ()
	{
        ins = this;
	}

    public void saveSteps(int _save)
    {
        _save = ScoreManager.score;
        XmlSerializer serializer = new XmlSerializer(typeof(int));
        FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/playerStats.xml", FileMode.Create);
        serializer.Serialize(stream, _save);
        stream.Close();
        
    }
}

[System.Serializable]               //Able to see it in the inspector
public class ItemEntry
{
    public string ItemName;
    public Material material;
    public int value;

    public enum Material
    {

    }
}