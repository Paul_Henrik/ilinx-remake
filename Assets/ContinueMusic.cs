﻿using UnityEngine;
using System.Collections;

public class ContinueMusic : MonoBehaviour {

	static bool AudioBegin = false;
	AudioSource audio;
	public AudioClip MenuMusic;

	void Awake ()
	{
		audio = GetComponentInChildren<AudioSource>();
		if (!AudioBegin)
		{
			audio.PlayOneShot(MenuMusic);
			DontDestroyOnLoad(gameObject);
			AudioBegin = true;
		}
	}

	void Update ()
	{
		if (Application.loadedLevelName == "MainLevel")
		{
			audio.Stop();
			AudioBegin = false;

		}
	}







}
