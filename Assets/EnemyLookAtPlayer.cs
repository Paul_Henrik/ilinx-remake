﻿using UnityEngine;
using System.Collections;

public class EnemyLookAtPlayer : MonoBehaviour {

	Vector3 dir;
	public Transform terget;
	float angle;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		dir = terget.position - transform.position;

		angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;

		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

	}
}
