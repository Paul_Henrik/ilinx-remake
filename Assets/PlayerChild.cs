﻿using UnityEngine;
using System.Collections;

public class PlayerChild : MonoBehaviour
{
    public float dampTime = 0.15f;
    public Transform target;
    public bool leftChild;
    public bool downChild;
    public bool upChild;
    public bool rightChild;
    public GameObject projectile;
    private Transform firepoint;
    public float fireRate = 0.2f;
    private bool canShoot = true;
    void Start()
    {
        getTarget();
        firepoint = gameObject.transform.GetChild(1);
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetAxis("Fire1") != 0 || Input.GetKey(KeyCode.RightShift)) && canShoot)
        {
            GameMaster.SoundFromInstance(0);
            //Instantiate(projectile, firepoint.position, firepoint.rotation);
            StartCoroutine("Shoot");
        }
    }

    void FixedUpdate()
    {
        if (FindObjectOfType<PlayerController>() != null)
        {
            if (target == null && !PlayerController.staying)
            {
                getTarget();
            }
            if (PlayerController.staying)
            {
                target = null;
            }
            else if (!PlayerController.staying)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, dampTime);
            }
        }


    }
    void getTarget()
    {
        if (FindObjectOfType<PlayerController>() != null)
        {
            if (leftChild)
                target = FindObjectOfType<PlayerController>().transform.GetChild(2);
            else if (rightChild)
                target = FindObjectOfType<PlayerController>().transform.GetChild(3);
            else if (upChild)
                target = FindObjectOfType<PlayerController>().transform.GetChild(4);
            else if (downChild)
                target = FindObjectOfType<PlayerController>().transform.GetChild(5);
            firepoint = gameObject.transform.GetChild(1);
        }
    }

    IEnumerator Shoot()
    {
        Instantiate(projectile, firepoint.position, firepoint.rotation);
        canShoot = false;
        yield return new WaitForSeconds(fireRate);
        canShoot = true;
    }
}