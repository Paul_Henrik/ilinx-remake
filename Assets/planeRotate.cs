﻿using UnityEngine;
using System.Collections;

public class planeRotate : MonoBehaviour
{
    public float rotate;
	// Use this for initialization
	void Start ()
	{
        transform.rotation *= Quaternion.Euler(0, 0, rotate);
    }

	// Update is called once per frame
	void Update ()
	{

	}
}