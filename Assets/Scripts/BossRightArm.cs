﻿using UnityEngine;
using System.Collections;

public class BossRightArm : MonoBehaviour {

	public GameObject redBullet;
	public GameObject greenBullet;
	public float fireRate;

	bool canShootRed = false;
	bool canShootGreen = false;
	bool canShootMixed = false;

	int x = 1;

	public GameObject player;
	public PlayerController targetPlayer;

	//targeting
	Vector3 dir;
	float angle;

	float nextTimeToSearch = 0;

	// Use this for initialization
	void Start () {
		//Picking target
		if (player.CompareTag ("red"))
			FindPlayer ();
		//targetPlayer = GameObject.FindGameObjectWithTag ("red").GetComponent<PlayerController> ();

		if(player.CompareTag("green"))
			FindPlayer ();
		//targetPlayer = GameObject.FindGameObjectWithTag ("green").GetComponent<PlayerController> ();
	}

	// Update is called once per frame
	void Update () {

		if (targetPlayer == null){
			FindPlayer ();
			return;
		}

		//Targeting
		dir = targetPlayer.transform.position - transform.position;
		angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis (angle +90, Vector3.forward);

	}

	public void StartShootingRed (){
		canShootRed = true;
		StartCoroutine (ShootPlayerWithRed ());
	}

	public void StopShootingRed (){
		canShootRed = false;
	}

	public void StartShootingGreen (){
		canShootGreen = true;
		StartCoroutine (ShootPlayerWithGreen ());
	}

	public void StopShootingGreen (){
		canShootGreen = false;
	}

	public void StartShootingMixed (){
		canShootMixed = true;
		StartCoroutine (ShootPlayerWithMixedRed ());
	}

	public void StopShootingMixed (){
		canShootMixed = false;
	}

	IEnumerator ShootPlayerWithRed (){

		Instantiate (redBullet, transform.position, transform.rotation);
		yield return new WaitForSeconds (fireRate);
		if (canShootRed)
			StartCoroutine (ShootPlayerWithRed ());

	}

	IEnumerator ShootPlayerWithGreen (){

		Instantiate (greenBullet, transform.position, transform.rotation);
		yield return new WaitForSeconds (fireRate);
		if (canShootGreen)
			StartCoroutine (ShootPlayerWithGreen ());

	}

	IEnumerator ShootPlayerWithMixedRed (){
		Instantiate (redBullet, transform.position, transform.rotation);
		yield return new WaitForSeconds (fireRate);
		if (canShootMixed)
			StartCoroutine (ShootPlayerWithMixedGreen ());
	}

	IEnumerator ShootPlayerWithMixedGreen (){
		Instantiate (greenBullet, transform.position, transform.rotation);
		yield return new WaitForSeconds (fireRate);
		if (canShootMixed)
			StartCoroutine (ShootPlayerWithMixedRed ());
	}

	void FindPlayer (){
		if (nextTimeToSearch <= Time.time) {
			GameObject searchResultRed = GameObject.FindGameObjectWithTag ("red");
			GameObject searchResultGreen = GameObject.FindGameObjectWithTag ("green");
			if (searchResultRed != null) {
				targetPlayer = searchResultRed.GetComponent<PlayerController> ();
			}
			if (searchResultGreen != null) {
				targetPlayer = searchResultGreen.GetComponent<PlayerController> ();
			}
			nextTimeToSearch = Time.time + 0.5f;
		}
	}

	IEnumerator OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.tag == "playerBullet"){
			GetComponent<SpriteRenderer> ().color = Color.blue;
			yield return new WaitForSeconds (0.1f);
			GetComponent<SpriteRenderer> ().color = Color.white;
		}
	}
}
