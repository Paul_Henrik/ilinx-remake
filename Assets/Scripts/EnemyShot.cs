﻿using UnityEngine;
using System.Collections;

public class EnemyShot : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {

		GetComponent<Rigidbody2D> ().velocity = transform.right * speed;
		StartCoroutine (DeleteShot());

	}

	IEnumerator DeleteShot(){
		yield return new WaitForSeconds (5);
		Destroy (this.gameObject);
	}

}
