﻿using UnityEngine;
using System.Collections;

public class BossSpreadShot : MonoBehaviour {

	public GameObject redBullet;
	public GameObject greenBullet;
	public float fireRate;

	bool canRedSpread = false;
	bool canGreenSpread = false;

	// Rotasjonshastighet
	public int rotateSpeed;

	// What way to spin
	bool right;
	bool left;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (!UIManager.isPaused) {

			if (right) {
				transform.RotateAround (transform.position, transform.forward, rotateSpeed);
			} else if (left) {
				transform.RotateAround (transform.position, transform.forward, -rotateSpeed);
			}
		}
	}

	public void StartRedSpread (){
		// Spin right
		right = true;
		left = false;

		canRedSpread = true;
		StartCoroutine (UseRedSpread ());
	}

	public void StopRedSpread (){
		canRedSpread = false;
	}

	public void StartGreenSpread (){
		// Spin left
		left = true;
		right = false;

		canGreenSpread = true;
		StartCoroutine (UseGreenSpread ());
	}

	public void StopGreenSpread (){
		canGreenSpread = false;
	}

	IEnumerator UseRedSpread (){
		Instantiate (redBullet, transform.position, transform.rotation);
		yield return new WaitForSeconds (fireRate);
		if (canRedSpread)
			StartCoroutine (UseRedSpread ());
	}

	IEnumerator UseGreenSpread (){
		Instantiate (greenBullet, transform.position, transform.rotation);
		yield return new WaitForSeconds (fireRate);
		if (canGreenSpread)
			StartCoroutine (UseGreenSpread ());
	}
}
