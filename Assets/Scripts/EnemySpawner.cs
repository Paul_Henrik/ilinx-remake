﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{

	public int spawnInterval;
	//seconds between each spawn event
	public int waveSize;
	//number of enemies per spawn event, can be adjusted in script to vary size between waves
	public GameObject[] enemies;
	//array of prefab enemies
	public float miliSec;
	//counts miliseonds
	public int sec;
	//counts seconds
	public int wave;
	//current enemy wave (spawn event)

	public GameObject boss;
	BossMain bossObject;
	public GameObject bossHealthBar;

	public GameObject warning;

	// Use this for initialization
	void Start ()
	{
		sec = 0;						//sets seconds to zero at game start
		miliSec = 0f;					//sets milisec to zero at game start
		wave = 1;						//sets wave to 1 at game start

//		warning = GameObject.FindWithTag ("warning");
//		warning.SetActive (false);
//		bossObject = GameObject.FindWithTag ("boss").GetComponent<BossMain> ();
//		boss = GameObject.FindWithTag ("boss");
//		boss.SetActive (false);
//		bossHealthBar = GameObject.FindWithTag ("bossHealthBar");
//		bossHealthBar.SetActive (false);
	}

	// Update is called once per frame
	void Update ()
	{
		if (!UIManager.isPaused) {

			miliSec += 1;					//+1 milisec every update cycle
			if (miliSec >= 60) { 				//+1 sec per 60 milisec
				miliSec = 0;
				sec++;
			}

			if (sec == spawnInterval) { //starts spawning event if timer == spawnInterval
				sec = 0;

				if (wave == 1 || wave == 2 || wave == 3) { 			//starts wave 1, 2, 3, swarmers
					spawnInterval = 1;
					waveSize = 2;
					for (int i = 0; i < waveSize; i++) { //spawns i amount of enemies. Enemy movement is controlled on the individual enemy prefab.
						Instantiate (enemies [i], gameObject.transform.position, gameObject.transform.rotation);
					}
					if (wave == 3)
						spawnInterval = 5;
					
					wave++;
				} else if (wave == 4) { 			//starts wave 4, Enemies
					waveSize = 2;
					for (int i = 0; i < waveSize; i++) { //spawns i amount of enemies. Enemy movement is controlled on the individual enemy prefab.
						Instantiate (enemies [i + 2], gameObject.transform.position, gameObject.transform.rotation);
					}
					wave++;
				} else if (wave == 5) { 			//starts wave 5, hunters
					waveSize = 2;
					for (int i = 0; i < waveSize; i++) { //spawns i amount of enemies. Enemy movement is controlled on the individual enemy prefab.
						Instantiate (enemies [i + 4], gameObject.transform.position, gameObject.transform.rotation);
					}
					wave++;
				} else if (wave == 6) { 			//starts wave 6, mix enemies-swarmer
					waveSize = 4;
					for (int i = 0; i < waveSize; i++) { //spawns i amount of enemies. Enemy movement is controlled on the individual enemy prefab.
						Instantiate (enemies [i + 6], gameObject.transform.position, gameObject.transform.rotation);
					}
					wave++;
				} else if (wave == 7) { 			//starts wave 7, mix enemies-hunters
					waveSize = 4;
					for (int i = 0; i < waveSize; i++) { //spawns i amount of enemies. Enemy movement is controlled on the individual enemy prefab.
						Instantiate (enemies [i + 10], gameObject.transform.position, gameObject.transform.rotation);
						print ("spawning " + i + 10);
					}
					wave++;
				} else if (wave == 8) { 			//starts wave 7, mix enemies-hunters
					//Instantiate (enemies [14], bossSpawner.position, bossSpawner.rotation);
//					boss.SetActive (true);
//					bossObject.BossIsActvated ();
//					bossHealthBar.SetActive (true);
//					warning.SetActive (true);
					wave++;
				}
			}
		} else {
			
		}
	}
}