﻿using UnityEngine;
using System.Collections;

public class DeleteParticles : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(DeleteParticleAfterTime ());
	}

	// Sletter partikklene etter en viss tid
	IEnumerator DeleteParticleAfterTime (){
		yield return new WaitForSeconds (5);
		Destroy (this.gameObject);
	}

}
