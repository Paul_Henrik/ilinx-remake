﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour {

	public static GameMaster gm;

	// Spiller-variabler
	public Transform playerPrefab;
	public Transform spawnPoint;
	public int spawnDelay;
	public static bool invouln = false;
	public int invoulnDuration;
	public GameObject respawnParticle;

	// Highscore
	public static int highScore;
	Text scoreText;
    private int currentLevel;
    public static bool raiseLevel;
    public int raiseDifficultyPointLimit;


    // Audio
    AudioSource playerAudio;
	public AudioClip[] soundFX;
	public AudioClip mainMusic;
	public AudioClip fanfare;

	// UI / Menu
	bool paused;

	// Use this for initialization
	void Start () {
        raiseLevel = false;
		ResetStats ();
        currentLevel = 0;
		
		gm = GameObject.FindGameObjectWithTag ("gm").GetComponent<GameMaster> ();

		scoreText = GameObject.Find ("scoreText").GetComponent<Text> ();

		playerAudio = GetComponent<AudioSource> ();

		GameObject.Find ("respawn").GetComponent<AudioSource> ().clip = mainMusic;
		GameObject.Find ("respawn").GetComponent<AudioSource> ().Play ();
	}
	
	// Update is called once per frame
	void Update () {

		// Oppdaterer Highscoren
		scoreText.text = "H i g h s c o r e : " + highScore;
        RaiseDifficulty();

	}

	// Fade til neste Scene
	IEnumerator FadeToNextLevel (){
		float fadetime = GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadetime);
		SceneManager.LoadScene("MainLevel");
	}

	// Fade og restart nåværende Scene
	IEnumerator RestartLevel (){
		float fadetime = GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadetime);
		ResetStats ();
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	// Reset alle stats
	void ResetStats (){
		highScore = 0;
		PlayerController.playerHealth = 3;
		PlayerController.playerLife = 3;
		invouln = false;
	}

	// Respawner Avataren
	public IEnumerator RespawnPlayer (){
		// Respawner avataren etter en spawndelay
		yield return new WaitForSeconds (spawnDelay);
		print ("---Spawned Player after " + spawnDelay + " seconds---");
		// Gjør spilleren uskadelig i en liten periode etter Respawn
		invouln = true;
		Instantiate (respawnParticle, spawnPoint.position, spawnPoint.rotation);
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
		yield return new WaitForSeconds (invoulnDuration);
		invouln = false;
	}
		
	public static void KillPlayer (PlayerController player){
		Destroy (player.gameObject);

		if (PlayerController.playerLife != 0) {
			print ("---Respawning Player---");

			gm.StartCoroutine (gm.RespawnPlayer());
		}
	}

	public static void KillBoss (BossMain boss){
		//SoundFromInstance (6);
		Destroy (boss.gameObject);
		gm.StartCoroutine (gm.Fanfare ());
		// Legger til poeng i Highscore
		AddToHighScore (200);
	}

	public static void KillEnemy (EnemyMove enemy){
		Destroy (enemy.gameObject);
		// Legger til poeng i Highscore
		AddToHighScore (20);
		SoundFromInstance (5);
	}

	public static void KillHunter (EnemyHunter enemy){
		Destroy (enemy.gameObject);
		// Legger til poeng i Highscore
		AddToHighScore (20);
		SoundFromInstance (5);
	}

    public static void KillZeppelin(Zeppelin enemy)
    {
        Destroy(enemy.gameObject);
        // Legger til poeng i Highscore
        AddToHighScore(20);
        SoundFromInstance(5);
    }

    public static void KillSwarmer (EnemySwarmer enemy){
		Destroy (enemy.gameObject);
		// Legger til poeng i Highscore
		AddToHighScore (20);
		SoundFromInstance (5);
	}

	IEnumerator Fanfare (){
		yield return new WaitForSeconds (1);
		GameObject.Find ("respawn").GetComponent<AudioSource> ().clip = fanfare;
		GameObject.Find ("respawn").GetComponent<AudioSource> ().Play ();
	}

	// Spiller av lyd fra en array med lydklipp
	public void PlaySoundClip (int intIn){
		if (!UIManager.isPaused)
		playerAudio.PlayOneShot (soundFX[intIn]);
	}

	public static void SoundFromInstance (int soundIn){
		gm.PlaySoundClip (soundIn);
	}

	public static void AddToHighScore (int scoreIn){
		highScore += scoreIn;
	}


    void RaiseDifficulty()
    {
        if (highScore >= currentLevel)
        {
            currentLevel = highScore + raiseDifficultyPointLimit;
            raiseLevel = true;
        }
    }
		
}
