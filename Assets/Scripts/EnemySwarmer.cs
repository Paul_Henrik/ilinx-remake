﻿using UnityEngine;
using System.Collections;

public class EnemySwarmer : MonoBehaviour
{
	//Decides what direction to bounce, true = original direction
	bool direction;
	//Horizontal speed
	public float horzSpeed;
	//Vertical speed
	public float vertSpeed;
	public GameObject deathParticle;

	// Helse til objektet
	public int enemyHealth;

	// Use this for initialization
	void Start ()
	{
		direction = true;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (horzSpeed, vertSpeed);
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (!UIManager.isPaused) {

			if (enemyHealth <= 0) {
				Instantiate (deathParticle, transform.position, transform.rotation);
				GameMaster.KillSwarmer (this);
			}

		}

		/*if (transform.position.x >= 14 || transform.position.x <= -10 || transform.position.y <= -10 || transform.position.y >= 20)
			GameMaster.KillSwarmer (this);*/
	}

	void OnTriggerEnter2D (Collider2D other)
	{

		if (other.gameObject.tag == "playerBullet") {
			enemyHealth--;
			Destroy (other.gameObject);
		}

		if (other.gameObject.tag == "wall" && direction == true) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-horzSpeed, vertSpeed);
			direction = false;
		} else if (other.gameObject.tag == "wall" && direction == false) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (horzSpeed, vertSpeed);
			direction = true;
		}
			
	}
}