﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour
{

    public GameObject targets;          //waypoint enemy will be moving to
    public int lifetime;                //how many seconds before enemy will move offscreen
    public int miliSec;                 //counts miliseconds
    public int endSec;                  //counts seconds, used to compare with lifetime to move enemy offscreen
    public float speed = 3;             //how fast enemy moves from spawn to waypoint
    public GameObject deathParticle;

    // Variabler foe skyting
    public GameObject bullet;
    public Transform firePoint;
    public float fireRate;

    // Rotasjonshastighet
    public int rotateSpeed;

    // Helse til objektet
    public int enemyHealth;


    // Use this for initialization
    void Start()
    {
        endSec = 0;                     //sets timer to 0
        StartCoroutine(Shoot());
    }

    // Update is called once per frame
    void Update()
    {

        if (!UIManager.isPaused)
        {

            miliSec++;

            if (enemyHealth <= 0)
            {
                Instantiate(deathParticle, transform.position, transform.rotation);
                GameMaster.KillEnemy(this);
            }

            transform.RotateAround(transform.position, transform.forward, rotateSpeed);

            if (miliSec >= 60)
            {
                miliSec = 0;
                endSec++;                   //+1 sec per 60 milisec
            }

            //if (transform.position != targets.transform.position && endSec < lifetime) { 		//moves enemy to target waypoint if enemy is not currently at the waypoint and endSec is not larger than lifetime
            //	transform.position = Vector2.MoveTowards (transform.position, targets.transform.position, speed);		//moves enemy to waypoint
            //} else {
            //	GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, -0.5f);				//moves enemy down and out of screen
            //}


        }
        else
        {

        }
    }

    // Funksjonen kaller seg selv opp etter "fireRate" sekunder
    IEnumerator Shoot()
    {

        Instantiate(bullet, firePoint.position, firePoint.rotation);
        yield return new WaitForSeconds(fireRate);
        StartCoroutine(Shoot());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "playerBullet")
        {
            StartCoroutine("takeDamage");
            enemyHealth--;
            Destroy(other.gameObject);

        }
    }

    IEnumerator takeDamage()
    {

        GetComponentInChildren<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.1f);
        GetComponentInChildren<SpriteRenderer>().color = Color.white;

    }
}
