﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    // Avatar variabler
    public float hSpeed = 8.0f;
    public float vSpeed = 3.0f;

    public float hS;
    public float vS;

    public float fireRate;
    public static bool canShoot = true;
    bool canNuke = true;
    bool canChange = true;
    Rigidbody2D body2D;
    public Sprite[] color;
    public GameObject deathParticle;
    public int smoothDrag;
    //Animator anim;

    //Child Variabler
    private Transform left;
    private Transform right;
    private Transform up;
    private Transform down;

    //child formation
    public static bool staying = false;
    private bool xFormation;

    CameraShake shake;
    public float shakeAmount;
    public float shakeLenght;

    // Variabler til skyting
    public GameObject firePoint;
    public GameObject projectile;

    // Nuke variabler
    public GameObject nuke;
    public int nukeMax;
    Text nukeText;

    // Internal timer, brukes til skudd
    int clock;

    // Ammunisjon
    public int ammunisjon;
    public int giveAmmo;
    public int maxAmmo;
    Text ammunisjonText;

    // Brukt for å bytte farge
    int x = 1;

    // Playerstats
    public float rightWall;
    public float leftWall;
    public static int playerHealth = 1;
    public static int playerLife = 3;
    Text healthText;
    Text lifeText;

    // Use this for initialization
    void Start()
    {
        GameMaster gm;
        shake = GameObject.Find("GM").GetComponent<CameraShake>();
        body2D = GetComponent<Rigidbody2D>();
        ammunisjonText = GameObject.Find("ammoText").GetComponent<Text>();
        nukeText = GameObject.Find("nukeText").GetComponent<Text>();
        healthText = GameObject.Find("health").GetComponent<Text>();
        playerHealth = 3;
        healthText.text = "H e a l t h : " + playerHealth;
        lifeText = GameObject.Find("life").GetComponent<Text>();
        lifeText.text = "L i v e s : " + playerLife;
        //anim = GetComponent<Animator> ();
        this.gameObject.tag = "red";
        //		gameObject.GetComponent<SpriteRenderer> ().color = Color.red;

        //get the position
        left = transform.GetChild(2);
        right = transform.GetChild(3);
        up = transform.GetChild(4);
        down = transform.GetChild(5);
        Formation();
    }

    void FixedUpdate()
    {

        // Horizontal & Vertical Controller Movement
        hS = hSpeed * Input.GetAxis("Horizontal");
        vS = hSpeed * Input.GetAxis("Vertical");

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            body2D.drag = 1;
            body2D.velocity = new Vector2(hS, vS);
            /*			if (Input.GetAxis ("Horizontal") < 0) {
                            anim.SetInteger ("animationstate", 2);
                        } else if (Input.GetAxis ("Horizontal") > 0) {
                            anim.SetInteger ("animationstate", 1);
                        } else {
                            anim.SetInteger ("animationstate", 0);
                        }
                    } else {
                        body2D.drag = smoothDrag;
                        anim.SetInteger ("animationstate", 0); */
        }

        //Formation Keyboard Input
        if (Input.GetKeyDown(KeyCode.Space))
            Formation();
        else if (Input.GetKeyUp(KeyCode.LeftAlt))
        {
            FormationStay();
        }

        // Horizontal & Vertical Keyboard Input
        if (Input.GetKey(KeyCode.D) && (transform.position.x < rightWall))
        {
            //body2D.drag = 1;
            body2D.velocity = new Vector2(hSpeed, body2D.velocity.y);
        }
        else if (Input.GetKey(KeyCode.A) && transform.position.x > leftWall)
        {
            //body2D.drag = 1;
            body2D.velocity = new Vector2(-hSpeed, body2D.velocity.y);
        }
        else if (Input.anyKey == false)
        {
            body2D.drag = smoothDrag;
        }

        if (Input.GetKey(KeyCode.W) && transform.position.y < 9f)
        {
            //body2D.drag = 1;
            body2D.velocity = new Vector2(body2D.velocity.x, vSpeed);
        }
        else if (Input.GetKey(KeyCode.S) && transform.position.y > -9f)
        {
            //body2D.drag = 1;
            body2D.velocity = new Vector2(body2D.velocity.x, -vSpeed);
        }
        else if (Input.anyKey == false)
        {
            body2D.drag = smoothDrag;
        }

    }

    // Update is called once per frame
    void Update()
    {
        //		healthText.text = "H e a l t h : " + playerHealth;
        //		lifeText.text = "L i v e s : " + playerLife;

        if (ammunisjon > maxAmmo)
        {
            ammunisjon = maxAmmo;
        }

        if (!UIManager.isPaused)
        {

            // Internal clock
            clock += 1;
            if (clock >= 60)
                clock = 0;

            //			ammunisjonText.text = "A m m o : " + ammunisjon;
            //			nukeText.text = "N u k e s : " + nukeMax;

            // Shooting
            if ((Input.GetAxis("Fire1") != 0 || Input.GetKey(KeyCode.RightShift)) && canShoot /*&& ammunisjon > 0*/)
            {
                GameMaster.SoundFromInstance(0);
                StartCoroutine(Shoot());
            }

            // NUKE
            if ((Input.GetAxis("Fire2") != 0 || Input.GetKeyDown(KeyCode.Return)) /*&& canNuke*/)
            {
                FormationStay();
                // Hvis avataren har fler enn 0 igjen
            /*    if (nukeMax > 0)
                {
                    GetComponent<NukeScript>().BlowUp();
                    nukeMax--;
                }
                canNuke = false; */
            }
            else if (Input.GetAxis("Fire2") == 0 && !canNuke)
            {
                canNuke = true;
            }

            // Bytte farge
            if ((Input.GetAxis("Fire3") != 0 || Input.GetKeyDown(KeyCode.Space))/* && canChange*/)
            {

                //ChangeColor (-1);
                //canChange = false;

            }
            else if (Input.GetAxis("Fire3") == 0 /*&& !canChange*/)
            {
                //canChange = true;
            }

        }
    }

    IEnumerator Shoot()
    {
        Instantiate(projectile, firePoint.transform.position, firePoint.transform.rotation);
        ammunisjon--;
        canShoot = false;
        yield return new WaitForSeconds(fireRate);
        canShoot = true;
    }

    // Bytting av farge
    //void ChangeColor (int cIn){

    //	x = (int)(x * (-1));
    //		if (x == 1) {
    //			this.gameObject.tag = "red";
    //		gameObject.GetComponent<SpriteRenderer> ().sprite = color [0];
    //		gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
    //		} else {
    //			this.gameObject.tag = "green";
    //		gameObject.GetComponent<SpriteRenderer> ().sprite = color [1];
    //		gameObject.GetComponent<SpriteRenderer> ().color = Color.green;
    //		}

    //}

    IEnumerator DamagePlayer()
    {

        playerHealth--;
        GameMaster.invouln = true;
        healthText.text = "Health: " + playerHealth;
        shake.Shake(shakeAmount, shakeLenght);
        GetComponentInChildren<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.1f);
        if (gameObject.tag == "red")
        {
            GetComponentInChildren<SpriteRenderer>().color = Color.white;
        }
        yield return new WaitForSeconds(1);
        GameMaster.invouln = false;
        yield return new WaitForSeconds(0.1f);
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "enemyBullet" || other.gameObject.tag == "enemy") 
        {
            if(other.gameObject.name != ("Zeppelin(Clone)"))
            Destroy(other.gameObject);
        GameMaster.SoundFromInstance(1);
        // Skjekker om avataren nettopp har Respawn'et
        if (!GameMaster.invouln)
        {
            StartCoroutine(DamagePlayer());
            print("---Lost 1 Health Point---");
            if (playerHealth == 0)
            {
                Instantiate(deathParticle, transform.position, transform.rotation);
                GameMaster.SoundFromInstance(2);
                playerLife--;
                //					lifeText.text = "Lives: " + playerLife;
                GameMaster.KillPlayer(this);
            }
        }
        }

    }

    public void Formation()
    {

        Vector3 newPos = transform.position;

        if (!xFormation)
        {
            left.position = newPos + new Vector3(-2f, 0.35f, 0);
            right.position = newPos + new Vector3(2f, 0.35f, 0);
            up.position = newPos + new Vector3(0, 2f, 0);
            down.position = newPos + new Vector3(0, -2f, 0);
            xFormation = true;
        }
        else if (xFormation)
        {
            left.position = newPos + new Vector3(0, 1f, +0);
            right.position = newPos + new Vector3(0, -1f, 0);
            up.position = newPos + new Vector3(0, -2f, 0);
            down.position = newPos + new Vector3(0, 2f, 0);
            xFormation = false;
        }
    }
    public void FormationStay()
    {
        staying = true;
        StartCoroutine("coFormationStay");
    }
    public IEnumerator coFormationStay()
    {
        yield return new WaitForSeconds(2);
        staying = false;
    }
}

// Gi ammo hvis objetet er av samme farge som avataren
//		if (other.gameObject.tag == "greenBullet" && this.gameObject.tag == "green") {
//			if (ammunisjon <= maxAmmo) 
//				ammunisjon += giveAmmo;

//				print ("---Picked up ammo---");
//				GameMaster.SoundFromInstance (4);
//				Destroy (other.gameObject);
//				GameMaster.AddToHighScore (1);

//		} else if (other.gameObject.tag == "redBullet" && this.gameObject.tag == "red") {
//			if (ammunisjon <= maxAmmo) 
//				ammunisjon += giveAmmo;

//				print ("---Picked up ammo---");
//				GameMaster.SoundFromInstance (4);
//				Destroy (other.gameObject);
//				GameMaster.AddToHighScore (1);


//			// Ta skade hvis objektet ikke er av samme farge som avataren}


// if (gameObject.tag == givenuke) {
//		nukeMax++;
// }
//	}
//
//}
