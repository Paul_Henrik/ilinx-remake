﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class BarScript : MonoBehaviour
{
	public float lerpSpeed;
	[SerializeField]
	private float fillAmount;

	public Image content;

	public Color fullColor;

	public Color lowColor;

	public float MaxValue { get; set; }

	public float Value {
		set {
			fillAmount = Map (value, 0, MaxValue, 0, 1);
		}
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		HandleBar ();
	}

	private void HandleBar ()
	{
		if (content.fillAmount != fillAmount) {
			content.fillAmount = Mathf.Lerp (content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
		}

		content.color = Color.Lerp (lowColor, fullColor, fillAmount);
	}

	private float Map (float value, float inMin, float inMax, float outMin, float outMax)
	{
		return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}

}
