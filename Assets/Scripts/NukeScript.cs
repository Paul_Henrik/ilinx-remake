﻿using UnityEngine;
using System.Collections;

public class NukeScript : MonoBehaviour {

	public GameObject nukeParticle;

	CameraShake shake;
	public float shakeAmount;
	public float shakeLenght;

	// Use this for initialization
	void Start () {
		shake = GameObject.Find ("GM").GetComponent<CameraShake> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BlowUp () {

		DestroyEnemies ();
		DestroyRedBullets ();
		DestroyGreenBullets ();

		shake.Shake (shakeAmount, shakeLenght);
		Instantiate (nukeParticle, transform.position, transform.rotation);

		GameMaster.SoundFromInstance(3);
	}

	void DestroyEnemies (){
		// Destroy all enemies on the screen
		GameObject[] enemyObjects;

		enemyObjects = GameObject.FindGameObjectsWithTag ("enemy");

		GameMaster.AddToHighScore (20*(enemyObjects.Length));

		for (int i= 0; i < enemyObjects.Length; i++){
			Destroy (enemyObjects[i]);
		}
	}
	void DestroyRedBullets (){
		// Destroy all red bullets on the screen
		GameObject[] enemyBullets;

		enemyBullets = GameObject.FindGameObjectsWithTag ("enemyBullet");

		GameMaster.AddToHighScore (1*(enemyBullets.Length));

		for (int i= 0; i < enemyBullets.Length; i++){
			Destroy (enemyBullets[i]);
		}
	}
	void DestroyGreenBullets (){
		// Destroy all green bullets on the screen
		GameObject[] enemyBullets2;

		enemyBullets2 = GameObject.FindGameObjectsWithTag ("enemyBullet");

		GameMaster.AddToHighScore (1*(enemyBullets2.Length));

		for (int i= 0; i < enemyBullets2.Length; i++){
			Destroy (enemyBullets2[i]);
		}
	}

}
