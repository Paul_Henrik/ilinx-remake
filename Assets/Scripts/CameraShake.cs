﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    public Camera mainCam;
	//public GameObject nukeParticle;

    float shakeAmount = 0;
	float ShakeAmount;
	float shakeLength;
    void Awake()
    {
        if (mainCam == null)
            mainCam = Camera.main;
    }
	
	// Use this for initialization
	void Start () {
		//gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 5);
		//StartCoroutine(BlowUp ());

	}

    //Funksjonens som setter igang shaking prosesen og stopper den
	public void Shake(float amt, float length)
    {
        shakeAmount = amt;
        InvokeRepeating("DoShake", 0, 0.01f);
        Invoke("StopShake", length);
       
    }
    //Starter shakingen
    void DoShake()
    {
        if(shakeAmount > 0)
        {
            Vector3 camPos = mainCam.transform.position;
            //Utrekningen som tilsier hvor mye camera skal shake
            float offsetX = Random.value * shakeAmount * 2 - shakeAmount;
            float offsetY = Random.value * shakeAmount * 2 - shakeAmount;
            camPos.x += offsetX;
            camPos.y += offsetY;

            mainCam.transform.position = camPos;
        }
    }
    //Stopper shakeingen
    void StopShake()
    {
        CancelInvoke("DoShake");
        mainCam.transform.localPosition = Vector3.zero;
    }

	IEnumerator BlowUp () {
		yield return new WaitForSeconds (1);
		GetComponent<CameraShake> ().Shake(ShakeAmount, shakeLength);
		//Instantiate (nukeParticle, transform.position, transform.rotation);

		// Destroy all enemies on the screen
		GameObject[] enemyObjects;

		enemyObjects = GameObject.FindGameObjectsWithTag ("enemy");

		GameMaster.AddToHighScore (20*(enemyObjects.Length));

		for (int i= 0; i < enemyObjects.Length; i++){
			Destroy (enemyObjects[i]);
		}

		GameMaster.SoundFromInstance(3);

		Destroy (this.gameObject);
	}

}
