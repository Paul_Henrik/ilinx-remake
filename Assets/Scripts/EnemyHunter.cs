﻿using UnityEngine;
using System.Collections;

public class EnemyHunter : MonoBehaviour
{

	public GameObject[] waypoints;
	public int waypointNumber;
	public int maxMoves;
	public float moveSpeed;
	public GameObject player;
	public GameObject deathParticle;
	public PlayerController targetPlayer;
	public float nextTimeToSearch = 0;

	//targeting
	Vector3 dir;
	float angle;

	// Variabler foe skyting
	public GameObject bullet;
	public Transform firePoint;
	public float fireRate;

	// Helse til objektet
	public int enemyHealth;

	// Use this for initialization
	void Start ()
	{

		//Picking target
		if (player.CompareTag ("red"))
			findTarget ();
			//targetPlayer = GameObject.FindGameObjectWithTag ("red").GetComponent<PlayerController> ();

		if (player.CompareTag ("green"))
			findTarget ();
			//targetPlayer = GameObject.FindGameObjectWithTag ("green").GetComponent<PlayerController> ();

		StartCoroutine (Shoot ());
		waypointNumber = 0;
	
	}

	// Update is called once per frame
	void Update ()
	{

		if (!UIManager.isPaused) {

			if (targetPlayer == null) {
				findTarget ();
				return;
			}

			//Targeting
			dir = targetPlayer.transform.position - transform.position;
			angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

			//Death
			if (enemyHealth <= 0) {
				Instantiate (deathParticle, transform.position, transform.rotation);
				GameMaster.KillHunter (this);
			}

			//Movement
			if (transform.position != waypoints [waypointNumber].transform.position)
				transform.position = Vector2.MoveTowards (transform.position, waypoints [waypointNumber].transform.position, moveSpeed);		//moves enemy to waypoint

			if (waypoints [waypointNumber].transform.position == transform.position)
				waypointNumber++;

			if (maxMoves <= waypointNumber)
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, 0);				//moves enemy left and out of screen
			
		}

		if (transform.position.x >= 14 || transform.position.x <= -10 || transform.position.y <= -10 || transform.position.y >= 20)
			GameMaster.KillHunter (this);
	}

	void findTarget ()
	{
		if (nextTimeToSearch <= Time.time) {
			GameObject searchResultRed = GameObject.FindGameObjectWithTag ("red");
			GameObject searchResultGreen = GameObject.FindGameObjectWithTag ("green");
			if (searchResultRed != null) {
				targetPlayer = searchResultRed.GetComponent<PlayerController> ();
			}
			if (searchResultGreen != null) {
				targetPlayer = searchResultGreen.GetComponent<PlayerController> ();
			}
			nextTimeToSearch = Time.time + 0.5f;
		}
	
	}
	// Funksjonen kaller seg selv opp etter "fireRate" sekunder
	IEnumerator Shoot ()
	{
		Instantiate (bullet, firePoint.position, firePoint.rotation);
		yield return new WaitForSeconds (fireRate);
		StartCoroutine (Shoot ());
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "playerBullet") {
			enemyHealth--;
            StartCoroutine("takeDamage");
			Destroy (other.gameObject);
		}
	}

    IEnumerator takeDamage()
    {
        GetComponentInChildren<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.1f);
        GetComponentInChildren<SpriteRenderer>().color = Color.white;
    }

}
