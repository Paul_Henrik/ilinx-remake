﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour
{
    
    public float scrollSpeed; //Variabelen som tilsier hvor fort Bakgrunden scroller
    public float tileSizeY; //En variabel som som til sier hvor stort bakgrunen er i Y-planet. Må sette inn riktig tall for at bakgrunen skal loppe riktig

    private Vector2 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        
        float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeY); //Regner ut hvor fort bakgrunen skal scrolle
        transform.position = startPosition + Vector2.down * newPosition; //Bruker varablen den fikk ovenfor med Vector2.down(som er det samme som 0.-1) til å får bakrunen til å scrolle
    }
}