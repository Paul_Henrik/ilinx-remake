﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 25);
		StartCoroutine(DeleteObject ());
	}

	// Update is called once per frame
	void Update () {
		
	}

	// Sletter skuddet etter en viss tid
	IEnumerator DeleteObject () {
		yield return new WaitForSeconds (1);
		Destroy (this.gameObject);
	}

}
