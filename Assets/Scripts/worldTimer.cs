﻿using UnityEngine;
using System.Collections;

public class worldTimer : MonoBehaviour {

	public int miliSec;
	public static int sec;
	public static int min;

	// Use this for initialization
	void Start () {
		sec = 0;
		min = 0;
	}
	
	// Update is called once per frame
	void Update () {


		miliSec += 1;
		if (miliSec >= 60) 
		{
			miliSec = 0;
			sec++;
		}
		if (sec >= 60) 
		{
			sec = 0;
			min++;
		}
	}
}
