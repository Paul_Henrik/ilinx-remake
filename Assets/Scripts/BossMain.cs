﻿using UnityEngine;
using System.Collections;

public class BossMain : MonoBehaviour {

	public Stat health;

	// Internal clock
	public int clock = 0;
	public int clockSec = 0;

	BossRightArm rightArm;
	BossLeftArm leftArm;
	BossSpreadShot spreadShot;

	public float cooldown;
	bool canDie = true;
	// Phases
	bool phase1 = true;
	bool phase2 = true;
	bool lastPhase = true;

	CameraShake shake;
	public float shakeAmount;
	public float shakeLenght;

	public int bossHealth;

	public GameObject bosshealthUI;
	public AudioClip bossMusic;
	public AudioClip warningSound;

	public GameObject warning;

	float nextTimeToSearch = 0;

	Animator anim;

	void Awake (){
		health.Initialize ();
		rightArm = GameObject.FindWithTag ("bossRightArm").GetComponent<BossRightArm>();
		leftArm = GameObject.FindWithTag ("bossLeftArm").GetComponent<BossLeftArm>();
		spreadShot = GameObject.FindWithTag ("bossSpreadShot").GetComponent<BossSpreadShot>();
		shake = GameObject.Find ("GM").GetComponent<CameraShake> ();
		anim = GetComponent<Animator> ();
	}

	// Use this for initialization
	void Start () {

	}

	void FixedUpdate (){
		
		// Internal clock
		clock += 1;
		if (clock >= 60) {
			clock = 0;
			clockSec++;
		}
		if (clockSec >= 30) {
			clockSec = 6;
		}
	} 
		
	// Update is called once per frame
	void Update () {
	
		if (!UIManager.isPaused) {

			// ---PHASE SHIFT / LAST 150HP---

			if (bossHealth <= 100 && lastPhase) {
				phase1 = true;
				phase2 = false;
				lastPhase = false;
				rightArm.StartShootingMixed ();
				leftArm.StartShootingMixed ();
			}

			// ---START PHASE 1---

			if (phase1){
				//phase2 = false;
				if (clockSec == 7 && clock == 0 && bossHealth >= 101) {
				spreadShot.StartGreenSpread ();
				} 
//				if (clockSec == 17 && clock == 0 || !phase1) {
//				spreadShot.StopGreenSpread ();
//				}

				if (clockSec == 8 && clock == 0 && bossHealth >= 101) {
				rightArm.StartShootingRed ();
				} 
//				if (clockSec == 18 && clock == 0 || !phase1) {
//				rightArm.StopShootingRed ();
//				}

				if (clockSec == 8 && clock == 0 && bossHealth >= 101) {
				leftArm.StartShootingRed ();
				} 
				if (clockSec == 18 && clock == 0 || !phase1) {
					phase1 = false;
					phase2 = true;
				}
				//phase2 = true;
			} 
			if (!phase1){
				spreadShot.StopGreenSpread ();
				rightArm.StopShootingRed ();
				leftArm.StopShootingRed ();
			}

			// ---START PHASE 2---

			if (phase2) {
				if (clockSec == 18 && clock == 0 && bossHealth >= 101) {
					spreadShot.StartRedSpread ();
				} 
//				if (clockSec == 28 && clock == 0 || !phase2) {
//					spreadShot.StopRedSpread ();
//				}

				if (clockSec == 19 && clock == 0 && bossHealth >= 101) {
					rightArm.StartShootingGreen ();
				} 
//				if (clockSec == 29 && clock == 0 || !phase2) {
//					rightArm.StopShootingGreen ();
//				}

				if (clockSec == 19 && clock == 0 && bossHealth >= 101) {
					leftArm.StartShootingGreen ();
				} 
				if (clockSec == 29 && clock == 0 || !phase2) {
					phase2 = false;
					phase1 = true;
				}
			}
			if (!phase2){
				spreadShot.StopRedSpread ();
				rightArm.StopShootingGreen ();
				leftArm.StopShootingGreen ();
			}

			// --- BOSS IS DEAD---

			if (bossHealth <= 0 && canDie) {
				canDie = false;
				rightArm.StopShootingMixed ();
				leftArm.StopShootingMixed ();

				StartCoroutine (BossIsDead ());
			}
		}

		warning = GameObject.FindWithTag ("warning");
	}

	IEnumerator BossIsDead (){
		shake.Shake (shakeAmount, shakeLenght);
		GameMaster.SoundFromInstance (6);
		GameObject.Find ("respawn").GetComponent<AudioSource> ().Stop ();
		anim.SetInteger ("animationstate", 1);
		leftArm.GetComponent<Animator> ().SetInteger ("animationstate", 1);
		rightArm.GetComponent<Animator> ().SetInteger ("animationstate", 1);
		spreadShot.GetComponent<Animator> ().SetInteger ("animationstate", 1);
		yield return new WaitForSeconds (1.4f);
		GameMaster.KillBoss (this);
	}

	public void BossIsActvated (){
		

		StartCoroutine (BossIsSpawned ());
	}

	IEnumerator BossIsSpawned (){
		//Instantiate (bosshealthUI, transform.position, transform.rotation);
		GameObject.Find ("respawn").GetComponent<AudioSource> ().clip = warningSound;
		GameObject.Find ("respawn").GetComponent<AudioSource> ().Play ();
		gameObject.GetComponent<Rigidbody2D> ().drag = 0;
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, -2);
		yield return new WaitForSeconds (6);
		warning.SetActive (false);
		GameObject.Find ("respawn").GetComponent<AudioSource> ().clip = bossMusic;
		GameObject.Find ("respawn").GetComponent<AudioSource> ().Play ();
		gameObject.GetComponent<Rigidbody2D> ().drag = 1;
		StartCoroutine (FloatUpAndDown ());
	}

	IEnumerator FloatUpAndDown (){

		gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (0, 1);
		yield return new WaitForSeconds (1);
		gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (0, -1);
		yield return new WaitForSeconds (1);
		StartCoroutine (FloatUpAndDown ());

	}

	IEnumerator OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.tag == "playerBullet"){
			bossHealth--;
			health.CurrentVal -= 1;
			Destroy (other.gameObject);
			GetComponent<SpriteRenderer> ().color = Color.blue;
			yield return new WaitForSeconds (0.1f);
			GetComponent<SpriteRenderer> ().color = Color.white;
		}
	}

}
