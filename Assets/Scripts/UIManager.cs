﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

	PlayerController playerController;

	GameObject[] pauseObjects;
	GameObject[] finishObjects;
	GameObject[] endObjects;

	public AudioClip fanfare;
	bool isPlaying = true;

	public static bool isPaused = false;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		pauseObjects = GameObject.FindGameObjectsWithTag ("ShowOnPause");
		finishObjects = GameObject.FindGameObjectsWithTag ("ShowOnFinish");
		endObjects = GameObject.FindGameObjectsWithTag ("endObjects");
		hidePaused();
		hideFinished();
		hideEnd ();
	}

	// Update is called once per frame
	void Update () {

		//for gameover meny
		if(PlayerController.playerLife <= 0){
			showFinished ();
		}

		//uses the p button to pause and unpause the game
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(Time.timeScale == 1)
			{
				Time.timeScale = 0;
				showPaused();
			} else if (Time.timeScale == 0){
				Debug.Log ("high");
				Time.timeScale = 1;
				hidePaused();
			}
		}

		if (GameObject.Find ("respawn").GetComponent<AudioSource> ().clip == fanfare && isPlaying) {
			isPlaying = false;

			showEnd ();
		} else {
		
		}

	}


	//Reloads the Level
	public void Reload(){
		
		Application.LoadLevel(Application.loadedLevel);

		if (SceneManager.GetActiveScene ().name == "MainLevel"){
			PlayerController.playerLife = 3;
			GameMaster.highScore = 0;
		}
	}

	//controls the pausing of the scene
	public void pauseControl(){
		if(Time.timeScale == 1)
		{
			Time.timeScale = 0;
			showPaused();
		} else if (Time.timeScale == 0){
			Time.timeScale = 1;
			hidePaused();
		}
	}

	//shows objects with ShowOnPause tag
	public void showPaused(){
		foreach(GameObject g in pauseObjects){
			g.SetActive(true);
			isPaused = true;
		}
	}

	//hides objects with ShowOnPause tag
	public void hidePaused(){
		foreach(GameObject g in pauseObjects){
			g.SetActive(false);
			isPaused = false;
		}
	}

	public void showFinished()
	{
		foreach (GameObject g in finishObjects) 
		{
			g.SetActive (true);
		}
	}

	public void hideFinished()
	{
		foreach (GameObject g in finishObjects) 
		{
			g.SetActive (false);
		}
	}

	//shows objects with ShowOnPause tag
	public void showEnd(){
		foreach(GameObject g in endObjects){
			g.SetActive(true);
		}
	}

	public void hideEnd()
	{
		foreach (GameObject g in endObjects) 
		{
			g.SetActive (false);
		}
	}

	//loads inputted level
	public void LoadLevel(string level){
		Application.LoadLevel(level);
	}

	public void ExitLevel(){
		Application.Quit ();
	}
}