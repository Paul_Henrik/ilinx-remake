﻿using UnityEngine;
using System.Collections;

public class bossHunterBullet : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {

		GetComponent<Rigidbody2D> ().velocity = -transform.up * speed;
		StartCoroutine (DeleteShot());

	}

	IEnumerator DeleteShot(){
		yield return new WaitForSeconds (5);
		Destroy (this.gameObject);
	}
}
